package autopark.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class NameController {
    @RequestMapping(value = "name")
    public String handle(ModelMap modelMap) {
        modelMap.put("name", "Zarema");
        modelMap.put("surname", "Zaynasheva");
        modelMap.put("age", "23");
        return "name.jsp";
    }
}
