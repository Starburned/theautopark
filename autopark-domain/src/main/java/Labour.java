/**
 * Created by vclass11 on 08.02.2016.
 */
public class Labour {
    private String name;
    private String surname;
    private String tel;
    public DateField( String label, int mode);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}

